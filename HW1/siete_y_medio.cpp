#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <ctime>
#include <cstdlib>
#include "cards.h"
using namespace std;

// Global constants (if any)


// Non member functions declarations (if any)


// Non member functions implementations (if any)


// Stub for main
int main(){
   /* --STATEMENTS-- */
   ofstream fout;
   fout.open("gamelog.txt");
   Player the_player = Player(100);
   Player the_dealer = Player(900);
   cout << "You have $100. Enter bet: ";
   int bet = 0;
   cin >> bet;
   cout << bet << endl;

   fout.close();
   return 0;
}
