# Assignment 1:

The purpose of this assignment is to practice using version control software by writing a game called Siete y Medio.

|**File**|**Description**|
|:------------:|:------------------------------------------------:|
| [`README.md`][read-me] | _readme_ file|
| [`.gitignore` ][ignore] |Types of files to be untracked by git|
| [`cards.h`][header-file] |Includes the function declarations for the classes Card, Hand, Player |
| [`cards.cpp`][cards] |Includes the function definitions |
| [`siete_y_medio.cpp`][main] |Includes the main function|


[read-me]: README.md
[ignore]: .gitignore
[header-file]: cards.h
[cards]: cards.cpp
[main]: siete_y_medio.cpp
